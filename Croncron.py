#!/usr/bin/env python3

from crontab import CronTab

planif = CronTab(user='ib')
execution = planif.new(command='python3 ssh_connect.py')
execution.minute.every(5)

planif.write()