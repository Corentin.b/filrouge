#!/usr/bin/env python3

import paramiko
import sys

destip = input('Adresse IP de la machine Ã  deployer:')
name = input('Nom administrateur de la machine:')
pword = input('Mot de passe administrateur de la machine:')

# La clef privÃ©e cÃ´tÃ© station
# id_rsa.pub correpondant doit Ãªtre dans le .ssh/authorized_keys
# du serveur destinataire
k = paramiko.RSAKey.from_private_key_file("/home/ib/.ssh/id_rsa")
c = paramiko.SSHClient()
# pas de warning si premiÃ¨re connexion
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Go !
c.connect(hostname=destip,username= name, password=pword)

# on rÃ©cupÃ¨re les flux std
c.invoke_shell()
stdin, stdout, stderr = c.exec_command('sudo apt-get install apache2')
# ce sont des fichiers (et des itÃ©rateurs)
print(stdout.read())

stdin, stdout, stderr = c.exec_command('sudo apt install apache2 php libapache2-mod-php mariadb-server php-mysql')
print(stderr.read())

stdin, stdout, stderr = c.exec_command('mysql -h 127.0.0.1 -u root -p')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('CREATE DATABASE wordpress ;')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('''CREATE USER 'wordpress'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'password';''')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('wget https://fr.wordpress.org/wordpress-latest-fr_FR.zip')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('sudo unzip wordpress-latest-fr_FR.zip -d /var/www')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('sudo unzip wordpress-latest-fr_FR.zip -d /var/www')
print(stdout.read())

stdin, stdout, stderr = c.exec_command('sudo chmod -R -wx,u+rwX,g+rX,o+rX /var/www/wordpress')
print(stdout.read())
