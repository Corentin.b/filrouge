#!/usr/bin/env python3

import paramiko
import sys

destip = input('Adresse IP de la machine Ã  deployer:')
name = input('Nom administrateur de la machine:')
pword = input('Mot de passe administrateur de la machine:')

# La clef privÃ©e cÃ´tÃ© station
# id_rsa.pub correpondant doit Ãªtre dans le .ssh/authorized_keys
# du serveur destinataire
k = paramiko.RSAKey.from_private_key_file("/home/ib/.ssh/id_rsa")
c = paramiko.SSHClient()
# pas de warning si premiÃ¨re connexion
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Go !
c.connect(hostname=destip,username= name, password=pword)

# on rÃ©cupÃ¨re les flux std
c.invoke_shell()
stdin, stdout, stderr = c.exec_command('sudo apt install mariadb-server')

# ce sont des fichiers (et des itÃ©rateurs)
print(stdout.read())
print(stderr.read())

stdin, stdout, stderr = c.exec_command('sudo apt install php-mysql')
print(stdout.read())
print(stderr.read())

stdin, stdout, stderr = c.exec_command('nano /etc/mysql/mariadb.conf.d/50-server.cnf')
print(stdout.read())
print(stderr.read())

stdin, stdout, stderr = c.exec_command('sudo systemctl restart mysql')
print(stdout.read())
print(stderr.read())