#!/usr/bin/env python3

import paramiko
import sys
import mysql.connector

destip = '192.168.105.53'

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="rootpw"
)

mycursor = mydb.cursor()
mycursor.execute("USE base;")

# La clef privÃ©e cÃ´tÃ© station
# id_rsa.pub correpondant doit Ãªtre dans le .ssh/authorized_keys
# du serveur destinataire
k = paramiko.RSAKey.from_private_key_file("/home/ib/.ssh/id_rsa")
c = paramiko.SSHClient()
# pas de warning si prem
# iÃ¨re connexion
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Go !
c.connect(hostname=destip,username='root')

# on rÃ©cupÃ¨re les flux std
c.invoke_shell()
stdin, stdout, stderr = c.exec_command(' uptime | cut -c32-')
load1 = stdout.read()
# ce sont des fichiers (et des itÃ©rateurs)
print(load1)

stdin, stdout, stderr = c.exec_command('date')
date1 = stdout.read()
print(date1)

stdin, stdout, stderr = c.exec_command('whoami')
user1 = stdout.read()
print(user1)

sql = "INSERT INTO info (load_average,date,user) VALUES (%s, %s, %s)"
val = (load1, date1, user1)
mycursor.execute(sql, val)